# Frontend Mentor - Interactive rating card component solution

This is a solution to the [Interactive rating card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/interactive-rating-component-koxpeBUmI). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements

This is a basic project, but it was done to:
* practicing fundamental concepts
* create my fist Vite project
* Deploy to Gitlab pages

### Links

- Live Site URL: [ive Solution](https://frontendchallenges.gitlab.io/interactive-rating/)

## My process

1. Create project `yarn vite create`
   Add dependencies `yarn add --dev sass`
2. Create tokens.scss with the constant values for colors, fonts, and sizes, and styles.scss for general styling
  The styles.scss must be included in main.js and tokens.scss in vite.config.ts
3. Set a backgroud color for body
4. Think of the design by splitting the layout in Components:
    Main:
      RatingCard
        Star image
        Card title
        Card body
          Text
          RatingNumberComponent (radio button)
          Submit button
      ThankComponent
        Header
          img
          badge
        Body
          title
          description
5. Create a card center vertically and horizontally
6. Fortunately mobile and desktop are similar. Inside the card set a flex layout, and several block elements.
7. Notice and implement the active states.
8. This challenge also requires to change the view when selecting the Submit button. Some people would modify the DOM structure, but I think it's cleaner to use router to change to the ThankComponent.
9. Deploy to gitlab pages (remember configure the base, e.g. vite build --base=./)

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- Vue + Vite
- VueRouter

### Lessons

To use the router, I installed vou-router, then used in the main.ts, create a router/index.js with the routes. To create the links, you can use router-link or programatically router.push
To build the rating buttons, I used a radio button group, with CSS, I hided the checkbox and showed only the labels (the numbers) and converted in a state button with the :checked pseudo.

```js
import { useRouter } from "vue-router";

const router = useRouter();
...
router.push({
      name: "thank-you",
      params: { rate: selectedRate.value },
    });
```

How to calculate the with of the image to use all the space avaiable, except for the margin.
```css
 &__chk {
    display: none;
  }
  &__lbl {
    display: block;
    width: $size;
    height: $size;
    line-height: $size;
    border-radius: 50%;
    transition: all 0.5s ease;
    cursor: pointer;
    background-color: $color-internal-elements;
  }
  &__lbl:hover {
    background-color: $color-primary-btn;
    color: $color-primary-white;
  }

  &__chk:checked + &__lbl {
    background-color: $color-primary-ligth-gray;
    color: $color-primary-white;
  }
```

### Continued development

This is one of many projects, I plan to build to make more experience with FrontEnd development, specially with the Framework, handling spacing and practicing css.

**Note: Delete this note and the content within this section and replace with your own plans for continued development.**

### Useful resources

- [How to deploy a Static Site](https://vitejs.dev/guide/static-deploy.html)
- [Router](https://next.router.vuejs.org/guide/essentials/passing-props.html#named-views)

## Author

- Website - [Víctor HG](https://gitlab.com/vianhg)
- Frontend Mentor - [@vianhg](https://www.frontendmentor.io/profile/vianhg)

## Acknowledgments

Always to God and my family.