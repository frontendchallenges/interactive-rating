import { createWebHistory, createRouter } from "vue-router";
import RatingCard from "../components/RatingCard.vue";
import ThankComponent from "../components/ThankComponent.vue";
import NotFoundComponent from "../components/NotFoundComponent.vue";

const routes = [
    {
        path: import.meta.env.BASE_URL + "/",
        name: "rating",
        component: RatingCard
    },
    {
        path: import.meta.env.BASE_URL + "/thank/:rate",
        name: "thank-you",
        component: ThankComponent,
        props: true
    },
    {
        path: "/:pathMatch(.*)",
        component: NotFoundComponent,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;