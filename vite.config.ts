import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig(({ command }) => ({
  plugins: [vue()],
  base: command === 'build' ? "/interactive-rating/" : "",
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
          @import "./src/tokens.scss";
        `
      }
    }
  }
}));
